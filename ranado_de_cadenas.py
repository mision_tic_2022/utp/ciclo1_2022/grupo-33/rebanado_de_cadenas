
#Acceder a un caracter
fruta = "Banana"

print( fruta[0], fruta[1] )

#rebanado de cadenas
#primera posicón incluye el caracter, última posición excluye
rebanado = fruta[0:2]
print(rebanado)

saludo = "Hola"

print( saludo[1: 6] )
#len -> Me permite obtener el tamaño de un String (cadena de caracteres)
tamanio = len(saludo)
print(tamanio)

ultimo_caracter = saludo[ tamanio-1 ]
print(ultimo_caracter)

ultimo_caracter = saludo[-3]
print(ultimo_caracter)